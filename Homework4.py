# Написать функцию которая принимает числа, выводит сумму чисел. Функцию надо вызвать.
def summa(num1, num2):
    print(num1 + num2)


summa(10, 5)


# Написать функцию которая принимает числа, выводит разность чисел. Функцию надо вызвать.
def raznost(num1, num2):
    print(num1 - num2)


raznost(1550, 1234)


# Написать функцию которая принимает числа, выводит произведение чисел. Функцию надо вызвать.
def proizvedenie(num1, num2):
    print(num1 * num2)


proizvedenie(9876, 12)


# Написать функцию которая принимает числа, выводит деление чисел. Функцию надо вызвать.
def delenie(num1, num2):
    print(num1 / num2)


delenie(12345, 123)


# Написать функцию которая принимает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.
def tsikl(massiv1):
    print(massiv1)
    for i in massiv1:
        print(i)


tsikl([1, 2, 3])

# Напишите приммеры использования всех операций с массивами
# len()
mass = [1, 2, 3, 4, 5, 6]
print()
print(len(mass))
# append()
mass.append(7)
print(mass)
#count()
vowels = ['a', 'b', 'c', 'd', 'e', 'f']
print(vowels.count('d'))
#copy()
shtoto = ['a', 'ab', 'ac']
n = shtoto.copy()
print(n)
print(n)
#extend()
spo1 = ['abc', 'abd', 'abe']
spo2 = ['def', 'deg', 'deh']
spo1.extend(spo2)
print('Sport', spo1)
#index()
top = ['s', 'd', 'f', 'g', 't']
d = top.index('f')
print()
print(d)
#remove('Meder')
cars = ['bmw', 'mers', 'audi']
cars.remove('mers')
print()
print(cars)
#reverse()
dvd = [1, 2, 3, 4, 5]
dvd.reverse()
print()
print(dvd)
#pop()
det = ['sul', 'adi', 'air', 'adil']
det.pop()
print()
print(det)
#clear()
det.clear()
print(det)

# Оберните все операции в функции, которые принимают масссив и выполняют над нимм операцию. Функцию надо вызвать.

def operation(mass):
    # len()
    print(len(mass))
    # append()
def operation2(mass):
    mass.append(7)
    print(mass)
    # count()
def operation3(mass):
    print(mass.count(6))
    # copy()
def operation4(mass):
    print(mass.copy())
    # extend()
def operation5(mass):
    mass.extend((7, 8, 9))
    print(mass)
    # index()
def operation6(mass):
    d = mass.index(3)
    print(d)
    # remove()
def operation7(mass):
    mass.remove(4)
    print(mass)
    # reverse()
def operation8(mass):
    mass.reverse()
    print()
    print(mass)
    # pop()
def operation9(mass):
    mass.pop()
    print()
    print(mass)
    # clear()
def operation10(mass):
    mass.clear()
    print()
    print(mass)


operation(["a", 1, 2, 3, 4, 5, 6])
operation2(["a", 1, 2, 3, 4, 5, 6])
operation3(["a", 1, 2, 3, 4, 5, 6])
operation4(["a", 1, 2, 3, 4, 5, 6])
operation5(["a", 1, 2, 3, 4, 5, 6])
operation6(["a", 1, 2, 3, 4, 5, 6])
operation7(["a", 1, 2, 3, 4, 5, 6])
operation8(["a", 1, 2, 3, 4, 5, 6])
operation9(["a", 1, 2, 3, 4, 5, 6])
operation10(["a", 1, 2, 3, 4, 5, 6])
